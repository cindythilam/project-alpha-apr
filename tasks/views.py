from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTask
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTask(request.POST)
        if form.is_valid():
            project = form.save()
            project.assignee = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = CreateTask()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    show_my_tasks = Task.objects.filter(assignee=request.user)
    context = {"show_my_tasks": show_my_tasks}
    return render(request, "tasks/list.html", context)
